package com.lkakulia.lecture_17

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var items = mutableListOf<UserModel.DataModel>()
    private lateinit var adapter: RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
    private lateinit var users: UserModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        parseJson()
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener({
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                parseJson()
                adapter.notifyDataSetChanged()
            }, 2000)
        })
    }

    private fun parseJson() {
        users = UserModel()
        val joson = JSONObject(json)
        val ad = joson.getJSONObject("ad")

        if (ad.has("company"))
            companyTextView.text = ad.getString("company")
        if(ad.has("url"))
            urlTextView.text = ad.getString("url")
        if(ad.has("text"))
            textTextView.text = ad.getString("text")

        if(joson.has("page"))
            users.page = joson.getInt("page")
        if(joson.has("per_page"))
            users.perPage = joson.getInt("per_page")
        if(joson.has("total"))
            users.total = joson.getInt("total")
        if(joson.has("total_pages"))
            users.totalPages = joson.getInt("total_pages")

        val jsonObjectArray = joson.getJSONArray("data")
        for (i in 0 until jsonObjectArray.length()) {
            val jsonObject = jsonObjectArray[i] as JSONObject
            val data = UserModel.DataModel()

            if(jsonObject.has("id"))
                data.id = jsonObject.getInt("id")
            if(jsonObject.has("email"))
                data.email = jsonObject.getString("email")
            if(jsonObject.has("first_name"))
                data.firstName = jsonObject.getString("first_name")
            if(jsonObject.has("last_name"))
                data.lastName = jsonObject.getString("last_name")
            if(jsonObject.has("avatar"))
                data.avatar = jsonObject.getString("avatar")

            users.data.add(data)
            items.add(data)
        }

    }

    val json = "{\"page\":2," +
            "\"per_page\":6," +
            "\"total\":12," +
            "\"total_pages\":2," +
            "\"data\":" +
            "[{\"id\":7," +
            "\"email\":\"michael.lawson@reqres.in\"," +
            "\"first_name\":\"Michael\"," +
            "\"last_name\":\"Lawson\"," +
            "\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg\"}," +
            "{\"id\":8," +
            "\"email\":\"lindsay.ferguson@reqres.in\"," +
            "\"first_name\":\"Lindsay\"," +
            "\"last_name\":\"Ferguson\"," +
            "\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg\"}," +
            "{\"id\":9," +
            "\"email\":\"tobias.funke@reqres.in\"," +
            "\"first_name\":\"Tobias\"," +
            "\"last_name\":\"Funke\"," +
            "\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg\"}," +
            "{\"id\":10," +
            "\"email\":\"byron.fields@reqres.in\"," +
            "\"first_name\":\"Byron\"," +
            "\"last_name\":\"Fields\"," +
            "\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg\"}," +
            "{\"id\":11," +
            "\"email\":\"george.edwards@reqres.in\"," +
            "\"first_name\":\"George\"," +
            "\"last_name\":\"Edwards\"," +
            "\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg\"}," +
            "{\"id\":12," +
            "\"email\":\"rachel.howell@reqres.in\"," +
            "\"first_name\":\"Rachel\"," +
            "\"last_name\":\"Howell\"," +
            "\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg\"}]," +
            "\"ad\":" +
            "{\"company\":\"StatusCode Weekly\"," +
            "\"url\":\"http://statuscode.org/\"," +
            "\"text\":\"A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things.\"}}"

    private fun refresh() {
        items.clear()
        adapter.notifyDataSetChanged()
    }

}
